# PRINTING WITHOUT DRIVERS

If your printer is networked and has port 9100 open, you can probably send PostScript to it and have it print. For example:

    nc printer 9100 < test.ps

You may need to kill nc with Control+C with some printers. (not tested well, had to with mine)

If you use LaTex, run latex file.tex; dvips file.dvi to make a PostScript file from your sources.

    pdf2ps file.pdf file.ps 
works     for PDF file conversion too.


^by TheMatrixShibe^