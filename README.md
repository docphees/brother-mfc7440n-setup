# INSTALLING BROTHER MFC7440N ON ARCH/ARCH-DERIVATES

AUTHOR: docphees

PLATFORM: Arch-Linux and Arch-like distros (Manjaro, Antergos...)

## Printer Installation

Put all files into a temporary directory.

You can use the GUI installer or install the printer manually.
Your computer, your choice.

### A) Using the GUI:

Simply run setup.sh or setup.py. It should be self explaining.

The GUI will install the driver package and setup the printer in CUPS.
The Address can be a quad or netbios name.

You can check if that worked by checking your CUPS printers:
[CUPS printer page](http://localhost:631/printers)

The GUI in action:

![screenshot](doc/screenshot-v1.1.1.png)

### B) Manual installation:

The driver package can be installed with the PAMAC GUI tool
("Install Local Packages" behind the menu/*burger* button).

#### CUPS setup
Go to your [CUPS printer page](http://localhost:631/printers) and add a new printer.

Description: "BROTHER MFC7440N"
Location: "Labor"
The device URI should be similar to either "lpd://BROTHA/BINARY_P1"
  OR "lpd://192.168.88.115/BINARY_P1"
  (try pinging your device first)

Make and Model: "Brother MFC7440N for CUPS"

