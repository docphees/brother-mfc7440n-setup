#!/bin/python3
#
# Brother MFC7440N installer for Linux
#
# Included is a driver software package from Brother Industries, Ltd
#
# v1.1.1
#


import tkinter as tk
import os
import sys
import glob


class Application(tk.Frame):
    def __init__(self):
        self.debug = True      # toggles debug messages

        self.root = tk.Tk()
        # root.geometry("40x40+100+200")

        self.root.resizable(False, False)
        self.root.title("printer installer")
        #self.root.overrideredirect(True)  # removes border

        self.chckDriver = int(0)
        self.chckNet = int(0)
        self.chckSetup = int(0)
        self.chckAll = int(0)
        self.chckInstalled = int(0)

        self.driverChoices = glob.glob("*.pkg.tar.xz")      # all possible driver packages
        self.driverName = tk.StringVar()
        if len(self.driverChoices) > 0:
            self.driverName.set(self.driverChoices[0])
        self.printerName = tk.StringVar("")
        self.printerDescription = tk.StringVar("")
        self.printerLocation = tk.StringVar("")
        self.printerAddress = tk.StringVar("")

        super().__init__(self.root)

        self.master.protocol("WM_DELETE_WINDOW", self.onClose)

        self.create_widgets()

        self.mainloop()

    def dbg(self, text=""):
        if self.debug:
            print("[DEBUG MSG]:" + str(text))

    def onClose(self):
        self.master.destroy()

    def cleanStr(self, dirty, relax=False):
        clean = str()
        alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789-."
        if relax:
            alphabet += " .,+;:()!?"
        for char in dirty:
            if char in alphabet:
                clean += char
        return clean

    def checkReady(self):
        # check if all is ready for install
        if self.chckDriver == 1 and self.chckNet == 1 and self.chckSetup ==1:
            self.chckAll = 1
            self.buttonInstall.configure(state="active")
        else:
            self.chckAll = 0
            self.buttonInstall.configure(state="disabled")

        if self.chckInstalled == 1:
            self.buttonInstall.configure(text="DONE", state="disabled")

    def checkSet(self, item, val):
        if val == 1:
            item.configure(text="[x]")
        else:
            item.configure(text="[ ]")

    def onCheckDriverClick(self):
        # check if Driver file is present
        self.dbg("CheckDriver: " + str(os.path.isfile("./%s" % self.driverName.get())))
        if os.path.isfile("./%s" % self.driverName.get()):
            self.chckDriver = 1
            self.labelDriverPkg.configure(text="driver package found")
        else:
            self.chckDriver = 0
            self.labelDriverPkg.configure(text="no driver package found!\n place it in:\n" + sys.path[0])
        self.checkSet(self.checkDriver, self.chckDriver)
        self.checkReady()

    def onCheckNetClick(self):
        # Check via Ping
        URI = self.cleanStr(self.printerAddress.get())
        self.printerAddress.set(URI)
        if len(URI) > 1:
            self.entryAddress.configure(bg="white")
            self.labelAddressCheck.configure(text="pinging address...", fg="black")
            ping_result = os.system("ping -t 3 -c 1 " + URI)
            self.dbg("ping result: " + str(ping_result))
            if ping_result == 0:
                self.labelAddressCheck.configure(text="OK", fg="darkgreen")
                self.chckNet = 1
            else:
                self.labelAddressCheck.configure(text="no reply", fg="darkred")
                self.chckNet = 0
        else:
            self.entryAddress.configure(bg="yellow")
            self.labelAddressCheck.configure(text="", fg="black")
            self.chckNet = 0
        self.checkSet(self.checkNet, self.chckNet)
        self.checkReady()

    def onCheckSetupClick(self):
        # Check for issues with Name, Description, Location fields
        self.printerName.set(self.cleanStr(self.printerName.get()))
        self.printerDescription.set(self.cleanStr(self.printerDescription.get(), relax=True))
        self.printerLocation.set(self.cleanStr(self.printerLocation.get(), relax=True))
        if self.printerName.get() != "":
            self.entryName.configure(bg="white")
            self.chckSetup = 1
        else:
            self.printerName.set("")
            self.printerDescription.set("")
            self.printerLocation.set("")
            self.entryName.configure(bg="yellow")
            self.chckSetup = 0
        self.checkSet(self.checkSetup, self.chckSetup)
        self.checkReady()

    def onInstallClick(self):
        installError = 0
        # repeat all checks:
        self.onCheckDriverClick()
        self.onCheckNetClick()
        self.onCheckSetupClick()
        self.checkReady()
        if self.chckAll == 1:
            self.install()
        self.checkReady()

    def install(self):
        installError = 0
        # INSTALL DRIVER PACKAGE VIA PACMAN
        self.dbg("Installing driver package")

        # build installation command
        command = 'gksudo "pacman --noconfirm -U ./%s"' % self.driverName.get()
        self.dbg("PACMAN command: " + command)
        try:
            result = os.system(command)
        except:
            self.dbg("Installation error")
            installError = 1
        else:
            self.dbg("Package installation result: " + str(result))
            if result != 0:
                installError = 2

        # SET UP DRIVER IN CUPS
        if installError == 0:
            self.dbg("Installing driver package")

            # build command string
            command = "lpadmin -p %s -E" % self.printerName.get()
            command += " -v lpd://%s/BINARY_1 -m MFC7440N.ppd" % self.printerAddress.get()
            if self.printerDescription.get() != "":
                command += " -D '%s'" % self.printerDescription.get()
            if self.printerLocation.get() != "":
                command += " -L '%s'" % self.printerLocation.get()

            command = 'gksudo "' + command + '"'
            self.dbg("Command: " + command)
            try:
                result = os.system(command)      # gksudo lead to issues, has been removed.
            except:
                self.dbg("Cups setup error")
                installError = 3
            else:
                self.dbg("Cups setup result: " + str(result))
                if result != 0:
                    installError = 4
        if installError == 0:
            self.chckInstalled = 1

    def onKeypress(self, event):
        # Key event caught.
        if event.char in ("q", "Q"):
            self.onClose()

    def create_widgets(self):
        self.grid()        #either pack or grid the app
        #self.pack()

        # TITLE
        self.labelTitle = tk.Label(self, relief="sunken", text="BROTHER MFC-7440N SETUP TOOL").grid(row=0, column=0, columnspan=3)

        # DRIVER
        self.labelDriver = tk.Label(self, text="====== DRIVER").grid(row=10, column=0, columnspan=3, sticky="W")


        self.menuDriverName = tk.OptionMenu(self, self.driverName, *self.driverChoices)
        self.menuDriverName.grid(row=11, column=0, columnspan=3, stick="WE")

        self.labelDriverPkg = tk.Label(self, text="unchecked")
        self.labelDriverPkg.grid(row=12, column=0, columnspan=2)
        self.buttonCheckDriver = tk.Button(self, text="Check", command=self.onCheckDriverClick)
        self.buttonCheckDriver.grid(row=12, column=2)

        # NETWORK
        self.l2 = tk.Label(self, text="====== NETWORK CONNECTION").grid(row=20, column=0, columnspan=3, sticky="W")

        self.labelAddress = tk.Label(self, text="Address").grid(row=21, column=0)
        self.entryAddress = tk.Entry(self, textvariable=self.printerAddress)
        self.entryAddress.grid(row=21, column=1, sticky="W")
        self.buttonCheckNet = tk.Button(self, text="Check", command=self.onCheckNetClick).grid(row=21, column=2, columnspan=2, sticky="W")
        self.labelAddressCheck = tk.Label(self, text="")
        self.labelAddressCheck.grid(row=22, column=2, columnspan=1)

        # SETUP
        self.l3 = tk.Label(self, text="====== SETUP PRINTER").grid(row=30, column=0, columnspan=3, sticky="W")

        self.labelName = tk.Label(self, text="Name").grid(row=31, column=0, columnspan=1, sticky="W")
        self.entryName = tk.Entry(self, textvariable=self.printerName)
        self.entryName.grid(row=31, column=1, sticky="W")
        self.labelDescription = tk.Label(self, text="Description").grid(row=32, column=0, columnspan=1, sticky="W")
        self.entryDescription = tk.Entry(self, textvariable=self.printerDescription)
        self.entryDescription.grid(row=32, column=1, sticky="W")
        self.labelLocation = tk.Label(self, text="Location").grid(row=33, column=0, columnspan=1, sticky="W")
        self.entryLocation = tk.Entry(self, textvariable=self.printerLocation)
        self.entryLocation.grid(row=33, column=1, sticky="W")

        self.buttonName = tk.Button(self, text="Set", command=self.onCheckSetupClick).grid(row=33, column=2, sticky="ES")

        # INSTALLATION
        self.l4 = tk.Label(self, text="====== INSTALLATION").grid(row=40,column=0, columnspan=3, sticky="W")

        self.checkDriver    = tk.Label(self, text="[ ]")
        self.checkDriver.grid(row=41, column=0)
        self.checkNet       = tk.Label(self, text="[ ]")
        self.checkNet.grid(row=42, column=0)
        self.checkSetup      = tk.Label(self, text="[ ]")
        self.checkSetup.grid(row=43, column=0)

        self.checkboxInstallDriver = tk.Label(self, text="Driver").grid(row=41, column=1, columnspan=2, sticky="W")
        self.checkboxInstallNet = tk.Label(self, text="Network").grid(row=42, column=1, columnspan=2, sticky="W")
        self.checkboxInstallName = tk.Label(self, text="Description").grid(row=43, column=1, columnspan=2, sticky="W")

        # INSTALL BUTTON
        self.buttonInstall = tk.Button(self, text="INSTALL", command=self.onInstallClick, state="disabled")
        self.buttonInstall.grid(row=50, column=0)

        self.buttonCancel = tk.Button(self, text="Cancel", command=self.onClose).grid(row=50, column=2)

        #self.master.bind("<Key>", self.onKeypress)

        #self.resizable(False, False)


if __name__ == "__main__":
    app = Application()



